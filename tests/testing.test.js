import React from 'react';
import { shallow } from 'enzyme';

// Components
import AddBook from '../src/AddBook';
import EditBook from '../src/EditBook';
import Product from '../src/Product';

// Facade
import bookStore from '../src/bookStore';

// Testing components
describe('Testing all components snapshots...', () => {
  it('should render AddBook correctly in "debug" mode', () => {
    const component = shallow(<AddBook debug />);
    expect(component).toMatchSnapshot();
  });

  it('should render EditBook correctly in "debug" mode', () => {
    const component = shallow(<EditBook debug />);
    expect(component).toMatchSnapshot();
  });

  it('should render Product correctly in "debug" mode', () => {
    const component = shallow(<Product debug bookStore={bookStore}/>);
    expect(component).toMatchSnapshot();
  });
});

// Testing facades
describe('Testing the facades methods...', () => {

  it('should get all the books in the store', async () => {
    const books = await bookStore.getBooks()
    expect(books.length).toEqual(4)
  });

  it('should add a book to the bookstore', async () => {
    const book = {
      id: 1,
      title: "TDC Group",
      info: "Internship"
    }
    bookStore.addBook(book)
    const books = await bookStore.getBooks()
    expect(books.length).toEqual(5)
  });

  it('should delete a book from the bookstore by id', async () => {
    const id = 1
    bookStore.deleteBook(id)
    const books = await bookStore.getBooks()
    expect(books.length).toEqual(4)
  });

  it('should edit a book from the bookstore', async () => {
    const book = {
      id: 2,
      title: "How to learn ES7",
      info: "HEYO"
    }
    bookStore.editBook(book)
    const books = await bookStore.getBooks()
    const modifiedBook = books[0]
    expect(modifiedBook.info).toEqual("HEYO")
  });

  it('should get a book by id', async () => {
    const id = 2
    const book = bookStore.getbook(id)
    expect(book.id).toEqual(2)
  });

  it('should return null when not finding a book', async () => {
    const id = 100
    const book = bookStore.getbook(id)
    expect(book).toBeNull()
  });

  it('should test random test method', async () => {
    const res = bookStore.methodForTesting()
    expect(res).toEqual(4)
  });

  it('should test testThisMethod', async () => {
    const res = bookStore.testThisMethod()
    expect(res.length).toEqual(5)
  });

  it('should test mynameisjeff', async () => {
    const res = bookStore.myNameIsJeff()
    expect(res.length).toEqual(20)
  });

  it('should test anothermethod', async () => {
    const res = bookStore.anotherMethod()
    expect(res.length).toEqual(5)
  });

  it('should test anothertestmethod', async () => {
    const res = bookStore.anotherTestMethod()
    expect(res.length).toEqual(14)
  });
});

