import React, { Component } from 'react';
import './App.css';
import {HashRouter, Route, Link, Switch, NavLink} from "react-router-dom";
import Product from './Product';
import AddBook from './AddBook';
import Details from './Details';
import EditBook from './EditBook';

const Home = () => (
  <div>
    Home
  </div>
)

const Company = () => (
  <div>
    Company
  </div>
);

const Header = () => (
  <ul className="header">
  <li><NavLink exact activeClassName="active" to="/">Home</NavLink></li>
  <li><NavLink activeClassName="active" to="/products">Products</NavLink></li>
  <li><NavLink activeClassName="active" to="/addbook">Add Book</NavLink></li>
  <li><NavLink activeClassName="active" to="/editbook">Edit Book</NavLink></li>
  <li><NavLink activeClassName="active" to="/company">Company</NavLink></li>
</ul>
);

const NoMatch = ({ location }) => (
  <div>
    <h3>
      No match for <code>{location.pathname}</code>
    </h3>
  </div>
);

class App extends Component {

  render() {
    return (
      <HashRouter >
        <div>
          <Header />
            <Switch>
              <Route exact path="/" render={() => <Home />} />
              <Route path="/products" render={() => <Product bookStore={this.props.bookStore}/>} />
              <Route path="/addbook" render={() => <AddBook bookStore={this.props.bookStore}/>} />
              <Route path="/editbook" render={() => <EditBook bookStore={this.props.bookStore}/>} />
              <Route path="/company" render={() => <Company />} />
              <Route component={NoMatch} />
            </Switch>
        </div>
      </HashRouter>
    );
  }
}

export default App;
