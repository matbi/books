import React, { Component } from 'react';
import {Route, Switch,Link} from 'react-router-dom';
import Details from './Details';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {books: []};

        this.handleDelete = this.handleDelete.bind(this);
    }

    async handleDelete(event) {
        let id = event.target.id;
        // delete the particular book
        let succesful = await this.props.bookStore.deleteBook(id);
        // retrieve the updated list of books
        let data = await this.props.bookStore.getBooks();
        this.setState({books: data});
    }

    async componentDidMount() {
        let data = await this.props.bookStore.getBooks();
        this.setState({books: data});
    }
    
    render() {
        var bookList = this.state.books.map(book => <li>{book.title}, 
                        <Link to={`/products/${book.id}`}>details</Link>,
                        <button id={book.id} onClick={this.handleDelete}>Delete</button>
                        </li>);
        return <div>
            <ul>
                {bookList}
            </ul>
            <Switch>
            <Route path={`/products/:id`} render={(props) => {
      	    return (<Details {...props} books={this.state.books} />)
            }} />
            <Route path={`/products`} render={() => {
      	    return (<div>Book details for selected book will go here</div> )
            }} />
            
            </Switch>
            <p>{JSON.stringify(this.state.books)}</p>
            </div>
    }
}

export default Product;