import React from 'react';
import {Prompt} from "react-router-dom";


class AddBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {id: 0, title: "", info: "", isBlocking: false}
        this.handleInputChange.bind(this)
        this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        let block = false;
        if(document.forms[0].elements[0].value.length > 0 || document.forms[0].elements[1].value.length > 0)
            block = true;

        const value = event.target.value;
        const name = event.target.name;
    
        this.setState({
          [name]: value,
          isBlocking: block
        });
    }

    handleSubmit(evt) {
        evt.preventDefault();
        this.props.bookStore.addBook(this.state);
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <Prompt
                        when={this.state.isBlocking}
                        message={location =>
                            `Are you sure you want to go to ${location.pathname}`
                        }
                    />
                    <label>
                    Title
                    <input name="title" value={this.state.title} onChange={this.handleInputChange} /><br />
                    </label>
                    <label>
                    Info
                    <input name="info" value={this.state.info} onChange={this.handleInputChange} /><br />
                    </label>
                    <button>Save</button>
                    <p>{JSON.stringify(this.state)}</p>
                </form>
            </div>
        )
    }
}

export default AddBook;