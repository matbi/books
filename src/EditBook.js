import React from 'react';
import {Prompt} from "react-router-dom";


class EditBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {id: "", title: "", info: ""}
        this.handleInputChange.bind(this)
        this.handleSubmit.bind(this)
    }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
    
        this.setState({
          [name]: value,
        });
    }

    handleSubmit(evt) {
        evt.preventDefault();
        this.props.bookStore.editBook(this.state);
      }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>
                    ID
                    <input name="id" value={this.state.id} onChange={this.handleInputChange} /><br />
                    </label>
                    <label>
                    Title
                    <input name="title" value={this.state.title} onChange={this.handleInputChange} /><br />
                    </label>
                    <label>
                    Info
                    <input name="info" value={this.state.info} onChange={this.handleInputChange} /><br />
                    </label>
                    <button>Edit Book</button>
                    <p>{JSON.stringify(this.state)}</p>
                </form>
            </div>
        )
    }
}

export default EditBook;