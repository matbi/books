import React from 'react';
const fetch = require("node-fetch");

const URL = "https://mathiasbigler.com/ReactAndRouting2-1.0-SNAPSHOT";

function handleHttpErrors(res) {
    if (!res.ok) {
      throw Error(res.statusText);
    }
    return res.json();
}

function makeOptions(type, data) {
    return {
      method: type,
      headers: {
        "Accept": "application/json",
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }
  }

class BookStore {
    constructor() {
      this._books = [
        { id: 1,title: "How to Learn JavaScript - Vol 1", info: "Study hard"},
        { id: 2,title: "How to Learn ES6", info: "Complete all exercises :-)"},
        { id: 3,title: "How to Learn React",info: "Complete all your CA's"},
        { id: 4,title: "Learn React", info: "Don't drink beers, until Friday (after four)"
        }
      ]
      this.getBooks = this.getBooks.bind(this);
      this._nextID= 5;
    }
    async getBooks() {
        return this._books;
        // this._books = fetch(URL + "/api/books").then(handleHttpErrors);
        // return this._books;
    }
    
    addBook(book){
      book.id = this._nextID;
      this._books.push(book);
      this._nextID++;

    //   var data = makeOptions("POST", book);
    //   fetch(URL + "/api/books", data);
    }

    deleteBook(id) {
        for (let index = 0; index < this._books.length; index++) {
            const element = this._books[index];
            if(element.id === Number(id)) {
                this._books.splice(index, 1);
            }
        }
        // var data = fetch(URL + "/api/books/"+id, {method: "delete"});
        // return data;
    }

    editBook(book) {
        for (let index = 0; index < this._books.length; index++) {
            const element = this._books[index];
            if(element.id === Number(book.id)) {
                book.id = parseInt(book.id);
                this._books[index] = book;
            }
        }
    //    var data = makeOptions("PUT", book);
    //    fetch(URL + "/api/books", data);
    }

    getbook(id) {
        for (let index = 0; index < this._books.length; index++) {
            const element = this._books[index];
            if(element.id === Number(id)) {
                return element
            }
        }
        return null
    }

    anotherMethod() {
        const user = {}
        return "hello"
    }

    methodForTesting() {
        const hey = "Heygit add"
        const result = 2 * 2
        return result
    }

    anotherTestMethod() {
        return "thisbetterwork"
    }

    myNameIsJeff() {
        var something = "something here yes"
        var something = "something here yes"
        var calc = 2 / 2
        return "Jeff knows this shit"
    }

    newMethod() {
        console.log("hehehehehe")
        console.log("hehehehehe")
        console.log("hehehehehe")
        console.log("hehehehehe")
        console.log("hehehehehe")
    }

    testThisMethod() {
        var arr = [2, 4, 6, 8, 10]
        arr = arr.map(element => element * 2)
        return arr
    }

    branchMethod() {
        console.log("yo, my name is fred")
    }

    branchMethod1() {
        console.log("yo, my name is fred")
    }

    bdfsdfranchMethod1() {
        console.log("yo, my name is fred")
    }

    Workplease() {
        console.log("yo, my name is fred")
    }
    
  }
  
  let bookStore = new BookStore();
  export default bookStore;