import React from 'react';

const Details = (props) => {
    let id = props.match.params.id;
    let book = props.books.find((book) => book.id === id);
    return <div>
            <p>Detailed info for the title, {book.title}</p>
            <p>ID: {book.id}</p>
            <p>Info: {book.info}</p>
           </div>
};

export default Details;

