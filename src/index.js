import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import BookStore from './bookStore';

ReactDOM.render(<App bookStore={BookStore}/>, document.getElementById('root'));
